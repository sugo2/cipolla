import { makeExecutableSchema } from "https://esm.sh/@graphql-tools/schema@9.0.15";
import * as postgres from "https://deno.land/x/postgres@v0.17.0/mod.ts";

import { Dinosaur, typeDefs } from "./types_definition.ts";

const connect = async () => {
  // Get the connection string from the environment variable "DATABASE_URL"

  const databaseUrl = Deno.env.get("DATABASE_URL")!;

  // Create a database pool with three connections that are lazily established
  const pool = new postgres.Pool(databaseUrl, 3, true);

  // Connect to the database
  const connection = await pool.connect();
  return connection;
};

const allDinosaurs = async () => {
  const connection = await connect();
  const result = await connection.queryObject`
            SELECT name, description FROM dinosaurs
          `;
  return result.rows;
};

const oneDinosaur = async (args: Dinosaur) => {
  const connection = await connect();
  const result = await connection.queryObject`
            SELECT name, description FROM dinosaurs WHERE name = ${args.name}
          `;
  return result.rows;
};

const addDinosaur = async (args: Dinosaur) => {
  const connection = await connect();
  const result = await connection.queryObject`
              INSERT INTO dinosaurs(name, description) VALUES(${args.name}, ${args.description}) RETURNING name, description
          `;
  return result.rows[0];
};

export const resolvers = {
  Query: {
    hello: () => `Hello World!`,
    allDinosaurs: () => allDinosaurs(),
    oneDinosaur: (_: Dinosaur, args: Dinosaur) => oneDinosaur(args),
  },
  Mutation: {
    addDinosaur: (_: Dinosaur, args: Dinosaur) => addDinosaur(args),
  },
};

export const schema = makeExecutableSchema({ resolvers, typeDefs });

import { gql } from "https://deno.land/x/graphql_tag@0.1.0/mod.ts";

export type Dinosaur = {
  name: string;
  description: string;
};

export const typeDefs = gql`
  type Query {
    hello: String
    allDinosaurs: [Dinosaur!]!
    oneDinosaur(name: String!): [Dinosaur!]!
  }
  type Dinosaur {
    name: String!
    description: String!
  }
  type Mutation {
    addDinosaur(name: String!, description: String!): Dinosaur!
  }
`;

import { Server } from "https://deno.land/std@0.175.0/http/server.ts";

// import { add } from "./functions.ts";
import { GraphQLHTTP } from "./mod.ts";
import { schema } from "./resolvers.ts";

// TODO: Move to Hono + GraphQL Middleware after v3.0.0 is released: https://github.com/honojs/hono/issues/736
// TODO: It should provide better performance and a better API

const server = new Server({
  handler: async (req) => {
    const { pathname } = new URL(req.url);

    return pathname === "/graphql"
      ? await GraphQLHTTP<Request>({
        schema,
        graphiql: true,
      })(req)
      : new Response("Not Found", { status: 404 });
  },
  port: 3000,
});

server.listenAndServe();

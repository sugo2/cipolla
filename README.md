# Cipolla: an API layer 🧅

## Prerequisites

- [Deno](https://deno.land/manual@v1.30.0/getting_started/installation)
- A PostgresSQL DB

## Run it

`env DATABASE_URL=postgresql://[pg_connection_string] deno run --allow-net --allow-env main.ts`

## Test it

todo!()

## Benchmark it

todo!()
